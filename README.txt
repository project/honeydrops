INSTALLATION
------------

 1. Download Honey Drops from https://www.drupal.org/project/honeydrops/

 2. Unpack the downloaded file, take the entire Honey Drops and place it in your
    Drupal installation under sites/all/themes. (Additional installation folders
    can be used; see https://drupal.org/getting-started/install-contrib/themes
    for more information.)

    For more information about acceptable theme installation directories, read
    the sites/default/default.settings.php file in your Drupal installation.

 3. Log in as an administrator on your Drupal site and go to the Appearance page
    at admin/appearance. You will see the Honey Drops listed under the Disabled
    Themes heading. You can optionally make Honey Drops the default theme.

 4. Now build your own sub-theme by set Honey Drops as the Base Theme.
 

SASS
----

Honey Drops is a Drupal theme & it can be compiled using CSS Preprocessor SASS.
You must to be setup SASS first in your project. Read more in http://sass-lang.com/


Font Awesome
------------

Font Awesome is used for icons. 
http://fontawesome.io/


Invert Logo
-----------

Basically in drupal sites we must upload favicon & site logo. Here I am introducing
another one option to upload the inverted logo to show in the menu. Configure this 
by accessing /admin/appearance/settings/honeydrops.


Admin Menu
----------
Admin menu orientation has been changed from landscape to portrait.
  -- Required modules:
  	 -- Administration Menu: https://www.drupal.org/project/admin_menu [Place this module in /sites/all/modules/contrib/]
  -- Modules to be disabled:
     -- Toolbar: D7 core module

