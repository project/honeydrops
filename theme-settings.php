<?php
function honeydrops_form_system_theme_settings_alter(&$form, $form_state, $key = '') {
	$form['inv_logo'] = array(
		'#type'          => 'fieldset',
		'#title'         => t('Inverted Logo image settings'),
		'#description'   => t("If toggled on, the following inverted logo will be displayed on the menu."),
    '#attributes' => array('class' => array('theme-settings-bottom')),
	);	
  $form['inv_logo']['default_inv_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use the default inverted logo'),
    '#default_value' => theme_get_setting('default_inv_logo', arg(3)),
    '#tree' => FALSE,
    '#description' => t('Check here if you want the theme to use the Inverted logo supplied with it.')
  );
  $form['inv_logo']['settings'] = array(
    '#type' => 'container',
    '#states' => array(
      // Hide the logo settings when using the default logo.
      'invisible' => array(
        'input[name="default_inv_logo"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['inv_logo']['settings']['inv_logo_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to custom inverted logo'),
    '#default_value' => theme_get_setting('inv_logo_path', arg(3)),
    '#description' => t('The path to the file you would like to use as your inverted logo file instead of the default logo.'),
  );
  $form['inv_logo']['settings']['inv_logo_upload'] = array(
    '#type' => 'file',
    '#title' => t('Upload Inverted logo image'),
    '#maxlength' => 40,
    '#description' => t("If you don't have direct file access to the server, use this field to upload your inverted logo.")
  );

  $form = system_settings_form($form);
  // We don't want to call system_settings_form_submit(), so change #submit.
  array_pop($form['#submit']);
  $form['#submit'][] = 'honeydrops_form_system_theme_settings_submit';
  $form['#validate'][] = 'honeydrops_form_system_theme_settings_validate';
  return $form;
}

/**
 * Validator for the system_theme_settings() form.
 */
function honeydrops_form_system_theme_settings_validate($form, &$form_state) {
  // Handle file uploads.
  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded logo.
  $file = file_save_upload('inv_logo_upload', $validators);
  if (isset($file)) {
    // File upload was attempted.
    if ($file) {
      // Put the temporary file in form_values so we can save it on submit.
      $form_state['values']['inv_logo_upload'] = $file;
    }
    else {
      // File upload failed.
      form_set_error('inv_logo_upload', t('The logo could not be uploaded.'));
    }
  }

  $validators = array('file_validate_extensions' => array('ico png gif jpg jpeg apng svg'));

  // If the user provided a path for a logo or favicon file, make sure a file
  // exists at that path.
  if (!empty($form_state['values']['inv_logo_path'])) {
    $path = honeydrops_theme_settings_validate_path($form_state['values']['inv_logo_path']);
    if (!$path) {
      form_set_error('inv_logo_path', t('The custom logo path is invalid.'));
    }
  }
}



function honeydrops_theme_settings_validate_path($path) {
  // Absolute local file paths are invalid.
  if (drupal_realpath($path) == $path) {
    return FALSE;
  }
  // A path relative to the Drupal root or a fully qualified URI is valid.
  if (is_file($path)) {
    return $path;
  }
  // Prepend 'public://' for relative file paths within public filesystem.
  if (file_uri_scheme($path) === FALSE) {
    $path = 'public://' . $path;
  }
  if (is_file($path)) {
    return $path;
  }
  return FALSE;
}


/**
 * Process system_theme_settings form submissions.
 */
function honeydrops_form_system_theme_settings_submit($form, &$form_state) {
  // Exclude unnecessary elements before saving.
  form_state_values_clean($form_state);

  $values = $form_state['values'];
  
  // Extract the name of the theme from the submitted form values, then remove
  // it from the array so that it is not saved as part of the variable.
  $key = $values['var'];
  unset($values['var']);
  
  // If the user uploaded a new logo or favicon, save it to a permanent location
  // and use it in place of the default theme-provided file.
  if (!empty($values['logo_upload'])) {
    $file = $values['logo_upload'];
    unset($values['logo_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_logo'] = 0;
    $values['logo_path'] = $filename;
    $values['toggle_logo'] = 1;
  }
  if (!empty($values['favicon_upload'])) {
    $file = $values['favicon_upload'];
    unset($values['favicon_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_favicon'] = 0;
    $values['favicon_path'] = $filename;
    $values['toggle_favicon'] = 1;
  }
  if (!empty($values['inv_logo_upload'])) {
    $file = $values['inv_logo_upload'];
    unset($values['inv_logo_upload']);
    $filename = file_unmanaged_copy($file->uri);
    $values['default_inv_logo'] = 0;
    $values['inv_logo_path'] = $filename;
  }

  // If the user entered a path relative to the system files directory for
  // a logo or favicon, store a public:// URI so the theme system can handle it.
  if (!empty($values['logo_path'])) {
    $values['logo_path'] = honeydrops_theme_settings_validate_path($values['logo_path']);
  }
  if (!empty($values['inv_logo_path'])) {
    $values['inv_logo_path'] = honeydrops_theme_settings_validate_path($values['inv_logo_path']);
  }
  if (!empty($values['favicon_path'])) {
    $values['favicon_path'] = honeydrops_theme_settings_validate_path($values['favicon_path']);
  }

  if (empty($values['default_favicon']) && !empty($values['favicon_path'])) {
    $values['favicon_mimetype'] = file_get_mimetype($values['favicon_path']);
  }

  variable_set($key, $values);

  cache_clear_all();
}

