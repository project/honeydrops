<?php

/**
 * Alter theme files
*/
function _honeydrops_theme_alter($files, $type) {
  $output = array();
  
  foreach($files as $key => $value) {
    if (isset($files[$key][$type])) {
      foreach ($files[$key][$type] as $file => $name) {
        $output[$name] = FALSE;
      }
    }
  }
  return $output;
}

/**
 * Getting theme information
 */
function honeydrops_theme_info($setting_name, $theme = NULL) {
  // If no key is given, use the current theme if we can determine it.
  if (!isset($theme)) {
    $theme = !empty($GLOBALS['theme_key']) ? $GLOBALS['theme_key'] : '';
  }
  $output = array();

  if ($theme) {
    $themes = list_themes();
    $theme_object = $themes[$theme];

    // Create a list which includes the current theme and all its base themes.
    if (isset($theme_object->base_themes)) {
      $theme_keys = array_keys($theme_object->base_themes);
      $theme_keys[] = $theme;
    }
    else {
      $theme_keys = array($theme);
    }

    foreach ($theme_keys as $theme_key) {
      if (!empty($themes[$theme_key]->info[$setting_name])) {
        $output[$setting_name] = $themes[$theme_key]->info[$setting_name];
      }
    }
  }
  
  return $output;
}

/**
 * Implements hook_css_alter().
 */
function honeydrops_css_alter(&$css) {
  // Load excluded CSS files from theme.
  $excludes = _honeydrops_theme_alter(honeydrops_theme_info('exclude'), 'css');
  $css = array_diff_key($css, $excludes);
}

/**
 * Implements hook_css_alter().
 */
function honeydrops_js_alter(&$javascript) {
  // Load excluded js files from theme.
  $excludes = _honeydrops_theme_alter(honeydrops_theme_info('exclude'), 'js');
  $javascript = array_diff_key($javascript, $excludes);
}

/**
 * Preprocessor for theme('textfield').
 */
function honeydrops_preprocess_textfield(&$vars) {
  if ($vars['element']['#size'] >= 30 && empty($vars['element']['#field_prefix']) && empty($vars['element']['#field_suffix'])) {
    $vars['element']['#size'] = '';
    if (!isset($vars['element']['#attributes']['class']) 
      || !is_array($vars['element']['#attributes']['class'])) {
       $vars['element']['#attributes']['class'] = array();
    }
    $vars['element']['#attributes']['class'][] = 'fluid';
  }
}

/**
 * Preprocessor for theme('password').
 */
function honeydrops_preprocess_password(&$vars) {
  if ($vars['element']['#size'] >= 30 && empty($vars['element']['#field_prefix']) && empty($vars['element']['#field_suffix'])) {
    $vars['element']['#size'] = '';
    if (!isset($vars['element']['#attributes']['class']) 
      || !is_array($vars['element']['#attributes']['class'])) {
       $vars['element']['#attributes']['class'] = array();
    }
    $vars['element']['#attributes']['class'][] = 'fluid';
  }
}

/**
 * Implements hook_preprocess_html().
 */
function honeydrops_preprocess_html(&$variables) {
	/**
	 * Admin menu Customization
	*/
  if (user_access('access administration menu')) {
  	drupal_add_js(path_to_theme() . '/js/adminmenu.js', 'file');
  	drupal_add_css(path_to_theme() . '/css/components/adminmenu.css', array('every_page' => TRUE));
  	drupal_add_js(path_to_theme() . '/js/jquery.mCustomScrollbar.concat.min.js', 'file');
  	drupal_add_css(path_to_theme() . '/css/jquery.mCustomScrollbar.min.css', array('every_page' => TRUE));
	}
	
	/**
	 * Chosen
	 */
	if(module_exists('chosen') && variable_get('chosen_use_theme', TRUE)) {
  	drupal_add_css(path_to_theme() . '/css/components/chosen-select.css', array('every_page' => TRUE));
  }
  
  /**
   * CTools
  */
  if(module_exists('ctools')) {
  	drupal_add_css(path_to_theme() . '/css/components/c-tools.css', array('every_page' => TRUE));
  }
   
  /**
   * ViewsUI
  */
  if(module_exists('views_ui')) {
  	drupal_add_css(path_to_theme() . '/css/components/views-ui.css', array('every_page' => TRUE));
  }
}


