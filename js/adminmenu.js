

/**
 * Apply JavaScript-based hovering behaviors.
 *
 * @todo This has to run last.  If another script registers additional behaviors
 *   it will not run last.
 */

Drupal.admin.behaviors.hover = function (context, settings, $adminMenu) {
	
	jQuery("#admin-menu").delegate(".expandable > a, .expandable > span", "click", function(e) {
  	e.preventDefault();
		jQuery(this).closest('.expandable').toggleClass('open');
		jQuery(this).next().slideToggle();
	});
	jQuery("#admin-menu").delegate(".expandable > a", "dblclick", function(e) {
		window.location.href = jQuery(this).attr('href');
	});
	
	jQuery("#admin-menu-wrapper").mCustomScrollbar({
		theme:"minimal"
	});
	
	jQuery(".menu-ico").click(function() {
		jQuery("body").toggleClass("menu-open");
	});
	
	jQuery('div > .dropdown > li > a').each(function() {
		if(!jQuery(this).closest('li').hasClass('admin-menu-action')) {
			var str = jQuery(this).text();
			var res = 'admin-menu-' + str.replace("/", "-");
			var classname = 'admin-menu-ico ' + res.toLowerCase();
			jQuery(this).closest('li').addClass(classname);
		}
		if(jQuery(this).closest('li').hasClass('admin-menu-icon')) {
			jQuery(this).text('Home');
		}
	});
	
	jQuery('#admin-menu').css('background-image', 'url('+jQuery('.inv-logo-img').attr('src')+')');
	jQuery('#admin-menu').removeClass('overlay-displace-top');
	
};
