<?php

/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 */
 global $theme_key;
?>
<div class="page-wrap">
	<div class="page">
		<header id="header" class="clearfix dplr">
		
			<div class="menu-ico head-ico fl rtl-fr hd"><i class="fa fa-bars" aria-hidden="true"></i></div>      
      <div class="search-bar head-ico fr rtl-fl lg-hd"><a href="#" class="search-link"><i class="fa fa-search" aria-hidden="true"></i></a></div>
		
      <?php if ($logo): ?>
        <div class="logo-wrp"><div class="logo-img"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a></div></div>
      <?php endif; ?>
      
      <?php if ($site_name): ?>
        <?php if ($title): ?>
          <div id="site-name" class="fl mn dml mt hd lg-sw"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></div>
        <?php else: /* Use h1 when the content title is empty */ ?>
          <h1 id="site-name" class="fl mn dml mt hd lg-sw"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a></h1>
        <?php endif; ?>
      <?php endif; ?>

      <?php if ($site_slogan): ?>
        <div id="site-slogan" class="fl dml dmt"><?php print $site_slogan; ?></div>
      <?php endif; ?>
      
      <?php if (theme_get_setting('default_inv_logo', $theme_key) == 1) :
      	$inv_logo = drupal_get_path('theme', $theme_key) . '/logo-invert.png';
      else:
      	$inv_logo = theme_get_setting('inv_logo_path', $theme_key);
      endif; ?>
    	<div class="inv-logo hd">
    		<a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
					<?php print theme('image', array('path' => $inv_logo, 'alt' => 'Home', 'title' => 'Home', 'attributes' => array('class' => 'inv-logo-img'))); ?>      		
    		</a>
    	</div>
      
		  <!-- <div class="main-menu fl dml nav-wrp">
		    <?php print theme('links__system_main_menu', array(
		      'links' => $main_menu,
		      'attributes' => array(
		        'id' => 'main-nav',
		        'class' => array('links nav'),
		      ),
		      'heading' => array(
		        'text' => t('Main menu'),
		        'level' => 'h2',
		        'class' => array('element-invisible'),
		      ),
		    )); ?>
		  </div>
		  
		  <div class="secondary-menu fl dml nav-wrp">
		    <?php print theme('links__system_secondary_menu', array(
		      'links' => $secondary_menu,
		      'attributes' => array(
		        'id' => 'sec-nav',
		        'class' => array('links nav'),
		      ),
		      'heading' => array(
		        'text' => t('Secondary menu'),
		        'level' => 'h2',
		        'class' => array('element-invisible'),
		      ),
		    )); ?>
		  </div> -->
      
      <?php print render($page['header']); ?>
      
      <div class="search-bar search-bar-lg dml dmt dpl fl bl hd lg-sw rtl-fr rtl-mln rtl-pln rtl-dmr rtl-dpr rtl-bln rtl-br"><a href="#" class="search-link"><i class="fa fa-search" aria-hidden="true"></i> <span class="search-desc">Type anywhere to <strong>search</strong></span></a></div>
      
		</header>
		
		<div id="main" class="pge-wdt clearfix">
			
			<?php print render($page['sidebar_first']); ?>
				
			<div class="main-content-wrap">
      	<div class="main-content">
			
					<?php if ($page['highlighted']): ?><div id="highlighted"><?php print render($page['highlighted']); ?></div><?php endif; ?>
					<?php if ($breadcrumb): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
					
			  	<div class="title-wrapper clearfix">
						<?php print render($title_prefix); ?>
						<?php if ($title): ?>
							<h1 class="title" id="page-title"><?php print $title; ?></h1>
						<?php endif; ?>
						<?php print render($title_suffix); ?>
						 
						<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
					</div>  
					
					<?php $tab = render($tabs); ?>
					<?php if ($tab): ?><div class="tabs"><?php print $tab; ?></div><?php endif; ?>
		
					<?php print $messages; ?>
		
					<?php print render($page['help']); ?>
					
					<?php print render($page['content']); ?>
					<?php print $feed_icons; ?>
				</div>
		    
			</div>
		    
		  <?php print render($page['sidebar_second']); ?>
			
		</div>
		
		<footer id="footer" class="pge-wdt">
    	<?php print render($page['footer']); ?>
		</footer>
	</div>
</div>

