/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.honeydrops = {
  attach: function(context, settings) {
  
	/**
	 * Search
	*/
	if($('.sch-blk').length) {
		$('.sch-blk .form-text').attr('placeholder', 'Type anywhere to search');
		$('.sch-blk').prepend('<i aria-hidden="true" class="fa fa-times"></i>');
		$('.search-bar, .sch-blk .fa').click(function() {
			$('body').toggleClass('sch-act');
		});
	} else {
		$('body').addClass('sch-dis');
	}
	
  }
};


})(jQuery, Drupal, this, this.document);
